# Responsiveness Demos

* `Demo1.java` Wrong way to handle long tasks: task blocks UI thread.

* `Demo2.java` Better way: task is broken up into smaller subtasks and still run in the UI thread.
				
* `Demo3.java` Best way: task runs in a different thread, and communicates with the UI thread in a safe way

