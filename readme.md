# CS 349 Demo Code

All the source code demonstrated during lectures will be posted to this repository. As a rule, code will be posted before the lecture, hopefully the night before or earlier. You should `git clone` this entire repository, and then `git pull` after each lecture. The code will likely be updated for bugs and improvements, so this way you'll always have the most up-to-date version.

> See the [course website](https://www.student.cs.uwaterloo.ca/~cs349/f17/) for lecture slides associated with these code demos.

## The code:

* [X Windows](x/)
* [Java](java/)

---

Bugs? Missing files? Fixes? Post to [Piazza](https://piazza.com/class/j6p05nk8vs52rt) with `lectures` tag.