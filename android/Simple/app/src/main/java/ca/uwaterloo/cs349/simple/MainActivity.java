package ca.uwaterloo.cs349.simple;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;

import android.graphics.Color;
import android.view.Menu;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d("DEMO", "MainActivity: onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // get a reference to widgets to manipulate on update
        final TextView number = (TextView)findViewById(R.id.numberField);

        SeekBar seek = (SeekBar)findViewById(R.id.seekBar);

        number.setText(Integer.toString(seek.getProgress()));

        // create a controller to increment counter when clicked
        seek.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                number.setBackgroundColor(Color.TRANSPARENT);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                number.setBackgroundColor(getResources().getColor(R.color.hilite_colour));
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                Log.d("DEMO", "MainActivity: onProgressChanged");
                if (progress == seekBar.getMax())
                    number.setText(R.string.at_max);
                else
                    number.setText(Integer.toString(progress));


            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
