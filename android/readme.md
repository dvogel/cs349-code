## Android Demos

> Note: all code requires [Android Studio](https://developer.android.com/studio) to compile and run

* Simple -- a first Android application

Three approaches to "classic" MVC in Android:

* MVC1 
* MVC2 
* MVC3 (uses older SDK version)

This example shows a more standard Android MVVM approach.

* MVVM