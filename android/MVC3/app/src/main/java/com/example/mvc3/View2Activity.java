package com.example.mvc3;

import java.util.Observable;
import java.util.Observer;

import com.example.mvc3.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class View2Activity extends Activity  {
	
	int n;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		Log.d("DEMO", "View2Activity: onCreate");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view2);
		
		// get a reference to widgets to manipulate on update
		TextView textview = (TextView)findViewById(R.id.view2_textview);
		
		// create a controller to increment counter when clicked
		textview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
	
				n++; // update the "model"
				update();
			}
		});
		
	}
	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		    n = extras.getInt("COUNT");
		}
		
		update();

	}
	
	// update the display 
	protected void update() {
		// get a reference to widgets to manipulate on update
		TextView textview = (TextView)findViewById(R.id.view2_textview);
    
	    StringBuilder s = new StringBuilder(n);
	    for (int i = 0; i < n; i++) {
	    	s.append("x");
	    }

		// update button text with click count
	    // (convert to string, or else Android uses int as resource id!)
	    textview.setText(s);	
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view2, menu);
		
		
		// get the menu item to add a listener
		MenuItem item = menu.findItem(R.id.menu_view2_gotoview1);
		
		// get the context (must be final to reference inside anonymous object)
		final View2Activity context = this;
		

		
		// create the menu item controller to change views
		item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				
				Intent intent = context.getIntent();
				intent.putExtra("COUNT", n);
				context.setResult(RESULT_OK, intent);

				finish();
				
				return false;
			}
		});
		return true;

	}
	


}
