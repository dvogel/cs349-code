package com.example.mvc3;

import android.util.Log;
import java.util.Observable;
import java.util.Observer;

public class Model extends Observable {

	static public final String STATE_COUNTER = "ModelCounter";

	private int counter;

	Model() {
		Log.d("DEMO", "Model: contructor");
		counter = 0;
	}

	// Data methods
	public int getCounterValue() {
		return counter;
	}

	public void setCounterValue(int i) {
		counter = i;
		Log.d("DEMO", "Model: set counter to " + counter);
		setChanged();
		notifyObservers();
	}

	public void incrementCounter() {
		counter++;
		Log.d("DEMO", "Model: increment counter to " + counter);
		setChanged();
		notifyObservers();
	}

	// Observer methods

	// a helper to make it easier to initialize all observers
	public void initObservers() {
		setChanged();
		notifyObservers();
	}

	@Override
	public void addObserver(Observer observer) {
		Log.d("DEMO", "Model: Observer added");
		super.addObserver(observer);
	}

	@Override
	public void notifyObservers() {
		Log.d("DEMO", "Model: Observers notified");
		super.notifyObservers();
	}


}