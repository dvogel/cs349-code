package com.example.mvc3;

import java.util.Observable;
import java.util.Observer;

import com.example.mvc3.R;
import com.example.mvc3.Model;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class View1Activity extends Activity implements Observer {

	Model model;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d("DEMO", "View1Activity: onCreate");

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_view1);

		// get a reference to widgets to manipulate on update
		Button button = (Button) findViewById(R.id.view1_button);

		// create a controller for the increment counter button
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// do this each time the button is clicked
				model.incrementCounter();
			}
		});

		// create model
		model = new Model();
		
		// add this activity to the model's list of observers
		model.addObserver(this);

		// initialize views
		model.initObservers();
	}


	@Override
	public void update(Observable observable, Object data) {
		Log.d("DEMO", "update View1");

		// get a reference to widgets to manipulate on update
		Button button = (Button) findViewById(R.id.view1_button);

		// update button text with click count
		// (convert to string, or else Android uses int as resource id!)
		button.setText(String.valueOf(model.getCounterValue()));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view1, menu);

		// get the menu item to add a listener
		MenuItem item = menu.findItem(R.id.menu_view1_gotoview2);

		// get the context (must be final to reference inside anonymous object)
		final Context context = this;

		// create the menu item controller to change views
		item.setOnMenuItemClickListener(new OnMenuItemClickListener() {

			@Override
			public boolean onMenuItemClick(MenuItem item) {

				// create intent to request the view2 activity to be shown
				Intent intent = new Intent(context, View2Activity.class);
				// pass some extra information
				intent.putExtra("COUNT", model.getCounterValue());
				// start the activity for result with request code 123
				startActivityForResult(intent, REQUEST_CODE);
				return false;
			}
		});
		return true;
	}

	// this is arbitrary
	final int REQUEST_CODE = 123;

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// get result and update the model
				model.setCounterValue(data.getIntExtra("COUNT", 0));
			} else if (resultCode == RESULT_CANCELED) {
				// could do something if cancelled

			}
		}
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {

		Log.d("DEMO", "View1Activity: onSaveInstanceState");

		// Save the model state
		savedInstanceState.putInt(Model.STATE_COUNTER, model.getCounterValue());

		// Always call the superclass so it can save the view hierarchy state
		super.onSaveInstanceState(savedInstanceState);
	}

	public void onRestoreInstanceState(Bundle savedInstanceState) {

		Log.d("DEMO", "View1Activity: onRestoreInstanceState");

		// Always call the superclass so it can restore the view hierarchy
		super.onRestoreInstanceState(savedInstanceState);

		// Restore state members from saved instance
		model.setCounterValue(savedInstanceState.getInt(Model.STATE_COUNTER));
	}

}
